# Regierungswechsel 2021 ~ Intro_DH

Repository beinhaltet den R Code und das Dataset, die für das Projekt genutzt wurden.

## Aufbau

- code: enthält den R Code, der zum Einlesen der Dateien und zum Erstellen des Topic Models dient
- dataset: 
    - cleaned data: enthält die mithilfe von Visiual Studio Code und regulären Ausdrücken gesäuberten Datensätze für die AfD und CDU
    - combined data: enthält die kombinierten Dateien auf Grundlage der rohen CSV-Dateien
    - raw data afd: enthält die rohen CSV-Dateien der AfD Plenarprotokolle, die durch die Volltextsuche der Open Discourse Website zur Verfügung gestellt wurden
    - raw data cdu: enthält die rohen CSV-Dateien der CDU/CSU Plenarprotokolle, die durch die Volltextsuche der Open Discourse Website zur Verfügung gestellt wurden
- plots: 
    - enthält die Visualisierungen der Analyseergebnisse des Topic Models
- resources:
    - enthält die Stoppwortliste und die Lemmaliste zur weiteren Verarbeitung des Topic Models
