
#---- Laden der Packages ----

library(quanteda)
library(quanteda.textstats)
library(topicmodels)
library(tidyverse)
library(LDAvis)
library(tsne)
library(ggplot2)
library(reshape2)
library(pals)
library(wordcloud2)
library(lsa)
library(ldatuning)

#---- Texte einlesen und formatieren ----

lemma_data <- read.csv("./resources/baseform_ger.csv", encoding = "UTF-8")
colnames(lemma_data)

# Von der Website:
# https://rdrr.io/github/trinker/lemmar/man/hash_lemma_de.html
# Zuletzt zugegriffen: 08.03.2023

stopwords_extended <- readLines("./resources/stopwords_ger.csv", encoding = "UTF-8")

plenary_data_afd <- read_csv("./dataset/cleaned data/AFD.csv")
colnames(plenary_data_afd)

plenary_data_cdu <- read_csv("./dataset/cleaned data/CDU.csv")
colnames(plenary_data_cdu)

# Von der Website:
# https://opendiscourse.de/volltextsuche
# Zuletzt zugegriffen: 08.03.2023

#---- Korpora erstellen ----

# AfD

plenary_corpus_afd <- corpus(plenary_data_afd$speech, docnames = plenary_data_afd$id)

# CDU

plenary_corpus_cdu <- corpus(plenary_data_cdu$speech, docnames = plenary_data_cdu$id)

# AfD

corpus_tokens_afd <- plenary_corpus_afd %>%
  tokens(remove_punct = TRUE, remove_numbers = TRUE, remove_symbols = TRUE) %>% 
  tokens_tolower() %>%
  tokens_replace(lemma_data$token, lemma_data$lemma,
                 valuetype = "fixed") %>%
  tokens_remove(pattern = stopwords_extended, padding = T) 

# CDU

corpus_tokens_cdu <- plenary_corpus_cdu %>%
  tokens(remove_punct = TRUE, remove_numbers = TRUE, remove_symbols = TRUE) %>% 
  tokens_tolower() %>%
  tokens_replace(lemma_data$token, lemma_data$lemma,
                 valuetype = "fixed") %>%
  tokens_remove(pattern = stopwords_extended, padding = T) 

#---- Kollokationsanalyse ----

# AfD

plenary_collocations_afd <- quanteda.textstats::textstat_collocations(corpus_tokens_afd, min_count = 25)

plenary_collocations_afd <- plenary_collocations_afd[1:11, ]

corpus_tokens_afd <- tokens_compound(corpus_tokens_afd, plenary_collocations_afd) 

# CDU

plenary_collocations_cdu <- quanteda.textstats::textstat_collocations(corpus_tokens_cdu, min_count = 25)

plenary_collocations_cdu <- plenary_collocations_cdu[1:11, ]

corpus_tokens_cdu <- tokens_compound(corpus_tokens_cdu, plenary_collocations_cdu) 

#---- Dokumentterm-Matrix ----

# AfD

DTM_afd <- corpus_tokens_afd %>%
  tokens_remove("") %>%  
  dfm() %>%      
  dfm_trim(min_docfreq = 3)

dim(DTM_afd)

#CDU

DTM_cdu <- corpus_tokens_cdu %>%
  tokens_remove("") %>%  
  dfm() %>%      
  dfm_trim(min_docfreq = 3)

dim(DTM_cdu)

#---- Daten säubern und Anzahl der Topics bestimmen ----

top20_terms <- c("deutschland", "Kollegin", "kollegen", "kollege",
                "damen", "herren", "sehr", "geehrte", "redezeit", "herr", 
                "frau", "herr_bundesminister", "präsidentin", "sprechen", "amt",
                "glauben", "meinung", "antrag", "lieben_kollegin", "Monat")

# AfD

DTM_afd <- DTM_afd[, !(colnames(DTM_afd) %in% top20_terms)]
sel_idx_afd <- rowSums(DTM_afd) > 0
DTM_afd <- DTM_afd[sel_idx_afd, ]
plenary_data_afd <- plenary_data_afd[sel_idx_afd, ]

result_afd <- FindTopicsNumber(
  DTM_afd,
  topics = seq(from = 2, to = 15, by = 1),
  metrics = c("Griffiths2004", "CaoJuan2009", "Arun2010", "Deveaud2014"),
  method = "Gibbs",
  control = list(seed = 77),
  mc.cores = 8L,
  verbose = TRUE
)

# Von der Website aus der Vorlesung:
# https://rpubs.com/siri/ldatuning
# Zuletzt zugegriffen: 08.03.2023

FindTopicsNumber_plot(result_afd)

#CDU

DTM_cdu <- DTM_cdu[, !(colnames(DTM_cdu) %in% top20_terms)]
sel_idx_cdu <- rowSums(DTM_cdu) > 0
DTM_cdu <- DTM_cdu[sel_idx_cdu, ]
plenary_data_cdu <- plenary_data_cdu[sel_idx_cdu, ]

result_cdu <- FindTopicsNumber(
  DTM_cdu,
  topics = seq(from = 2, to = 15, by = 1),
  metrics = c("Griffiths2004", "CaoJuan2009", "Arun2010", "Deveaud2014"),
  method = "Gibbs",
  control = list(seed = 77),
  mc.cores = 8L,
  verbose = TRUE
)

# Von der Website aus der Vorlesung:
# https://rpubs.com/siri/ldatuning
# Zuletzt zugegriffen: 08.03.2023

FindTopicsNumber_plot(result_cdu)

#---- Topic Model ----

# AfD

K_afd <- 12

topicModel_afd <- LDA(DTM_afd, K_afd, method="Gibbs", control=list(
  iter = 2000,
  seed = 1,
  verbose = 25,
  alpha = 0.02))

tmResult_afd <- posterior(topicModel_afd)

attributes(tmResult_afd)

ncol(DTM_afd)
beta_afd <- tmResult_afd$terms
dim(beta_afd)
rowSums(beta_afd)

nrow(DTM_afd)
theta_afd <- tmResult_afd$topics
dim(theta_afd)
rowSums(theta_afd)[1:12]

terms(topicModel_afd, 10)

topicNames_afd <- c("Unternehmen, EU und Koalition",
                    "Digitalisierung, Wirtschaft und Infrastruktur",
                    "Gesellschaft, Kultur und Werte",
                    "Arbeitnehmer, Rente und Geld",
                    "Energiewende, Strom und Landwirtschaft",
                    "Außenpolitik",
                    "Migration, Polizei und Islam",
                    "Familie und Bildung",
                    "Corona und Impfpflicht",
                    "Haushalt, Steuern und Schulden",
                    "Demokratie, Grundgesetz und Wahlen",
                    "Bundeswehr, Afghanistan und Sicherheit")

saveRDS(topicModel_afd, "topicModel_afd.rds")

# CDU

K_cdu <- 12

topicModel_cdu <- LDA(DTM_cdu, K_cdu, method="Gibbs", control=list(
  iter = 2000,
  seed = 1,
  verbose = 25,
  alpha = 0.02))

tmResult_cdu <- posterior(topicModel_cdu)

attributes(tmResult_cdu)

ncol(DTM_cdu)
beta_cdu <- tmResult_cdu$terms
dim(beta_cdu)
rowSums(beta_cdu)

nrow(DTM_cdu)
theta_cdu <- tmResult_cdu$topics
dim(theta_cdu)
rowSums(theta_cdu)[1:12]

terms(topicModel_cdu, 10)

topicNames_cdu <- c("Digitalisierung, Bildung und Haushalt",
                    "EU, Menschenrechte und Stärke",
                    "Unternehmen, Beratung und Grundgesetz",
                    "Arbeitsmarkt, Wirtschaft und Soziales",
                    "Mobilität, ländlicher Raum und Landwirtschaft",
                    "Bundeswehr und Sicherheit",
                    "Familie, Unterstützung und Kultur",
                    "Klimaschutz, Wirtschaft und Nachhaltigkeit",
                    "Rente, Pflege und Versorgung",
                    "Haushalt, Schulden und Entlastung",
                    "Polizei, Gewalt und Meinungsfreiheit",
                    "Corona und Impfpflicht")  

saveRDS(topicModel_cdu, "topicModel_cdu.rds")

#---- LDAvis browser ----

# AfD

svd_tsne_afd <- function(x) tsne(svd(x)$u)
json_afd <- createJSON(phi = beta_afd, theta = theta_afd, doc.length = rowSums(DTM_afd),
                   vocab = colnames(DTM_afd), term.frequency = colSums(DTM_afd), mds.method = svd_tsne_afd,
                   plot.opts = list(xlab = "", ylab = ""))
serVis(json_afd)

# CDU

svd_tsne_cdu <- function(x) tsne(svd(x)$u)
json_cdu <- createJSON(phi = beta_cdu, theta = theta_cdu, doc.length = rowSums(DTM_cdu),
                           vocab = colnames(DTM_cdu), term.frequency = colSums(DTM_cdu), mds.method = svd_tsne_cdu,
                           plot.opts = list(xlab = "", ylab = ""))
serVis(json_cdu)

#---- Begriffe zur Interpretation der Daten filtern ----

# AfD

topicToFilter_afd <- 1 
topicThreshold_afd <- 0.1
selectedDocumentIndexes_afd <- (theta_afd[, topicToFilter_afd] >= topicThreshold_afd)
filteredCorpus_afd <- plenary_data_afd$speech[selectedDocumentIndexes_afd]
filteredCorpus_afd[1]

# CDU

topicToFilter_cdu <- 1 
topicThreshold_cdu <- 0.1 
selectedDocumentIndexes_cdu <- (theta_cdu[, topicToFilter_cdu] >= topicThreshold_cdu)
filteredCorpus_cdu <- plenary_data_cdu$speech[selectedDocumentIndexes_cdu]
filteredCorpus_cdu[1]

#---- Liniendiagramm zur Bestimmung des Verhältnis der Topics pro Jahr ----

# AfD

plenary_data_afd$decade <- paste0(substr(plenary_data_afd$date, 0, 4))
topic_proportion_per_decade_afd <- aggregate(theta_afd, by = list(decade = plenary_data_afd$decade), mean)

colnames(topic_proportion_per_decade_afd)[2:(K_afd+1)] <- topicNames_afd

vizDataFrame_afd <- melt(topic_proportion_per_decade_afd, id.vars = "decade")

ggplot(vizDataFrame_afd,
       aes(x=decade, y=value, group=variable, color=variable)) +
  geom_line(linewidth = 1, linetype = "solid") + ylab("proportion") +
  geom_point(size = 2)+
  scale_color_manual(values = c("#FF6100","#00CFFF",
                                "#D287FF","#FFB887",
                                "#90D300","#FF0000",
                                "#9B00FF","#0000FF",
                                "#FF88D5","#88CAFF",
                                "#FFA511","#0085FF"), name = "Topics") +
  theme(axis.text.x = element_text(angle = 90, hjust = 1),
        panel.grid.major = element_blank(),
        panel.grid.minor = element_blank(),
        panel.background = element_blank(),
        axis.line = element_line(colour = "black"))

# CDU

plenary_data_cdu$decade <- paste0(substr(plenary_data_cdu$date, 0, 4))
topic_proportion_per_decade_cdu <- aggregate(theta_cdu, by = list(decade = plenary_data_cdu$decade), mean)

colnames(topic_proportion_per_decade_cdu)[2:(K_cdu+1)] <- topicNames_cdu

vizDataFrame_cdu <- melt(topic_proportion_per_decade_cdu, id.vars = "decade")

ggplot(vizDataFrame_cdu,
       aes(x=decade, y=value, group=variable, color=variable)) +
  geom_line(linewidth = 1, linetype = "solid") + ylab("proportion") +
  geom_point(size = 2)+
  scale_color_manual(values = c("#0000FF","#750049",
                                "#FF6100","#00CFFF",
                                "#90D300","#0085FF",
                                "#D287FF","#FF00A2",
                                "#FFB887","#88CAFF",
                                "#9B00FF","#FF88D5"), name = "Topics") +
  theme(axis.text.x = element_text(angle = 90, hjust = 1),
        panel.grid.major = element_blank(),
        panel.grid.minor = element_blank(),
        panel.background = element_blank(),
        axis.line = element_line(colour = "black"))

